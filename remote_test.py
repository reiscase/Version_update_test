import git
import os
__version__ = '0e1369adc0897186e5dc3f4736297dbbccb7a68d'

def check_for_updates():
    repo_dir = './repo'
    
    # Check if the repository already exists
    if not os.path.exists(repo_dir):
        # If it doesn't exist, clone the repository
        repo = git.Repo.clone_from('https://gitlab.com/reiscase/Version_update_test.git', './repo')
    else:
        # If it exists, open the repository
        repo = git.Repo(repo_dir)

    #fetch the latest changes from the remote repository
    repo.remotes.origin.fetch()

    # Get the latest commit hash
    latest_commit = repo.remotes.origin.refs['main'].commit.hexsha

    # Compare the latest commit hash with the current commit hash
    if latest_commit != repo.head.commit.hexsha:
        # Update the local repository to the latest commit
        repo.remotes.origin.pull()
        print('Updated to the latest version')
    else:
        print('Already up to date')

    #Return the latest commit hash
    return latest_commit

check_for_updates()
